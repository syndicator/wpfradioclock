﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using DigitalClock.Annotations;

namespace DigitalClock.ViewModels
{
    class MainViewModel : INotifyPropertyChanged
    {
        private String _monClock0;

        public String MonClock0
        {
            get { return _monClock0; }
            set
            {
                _monClock0 = value;
                OnPropertyChanged(nameof(MonClock0));
            }
        }
        private String _tuesClock0;

        public String TuesClock0
        {
            get { return _tuesClock0; }
            set
            {
                _tuesClock0 = value;
                OnPropertyChanged(nameof(TuesClock0));
            }
        }
        private String _wedClock0;

        public String WedClock0
        {
            get { return _wedClock0; }
            set
            {
                _wedClock0 = value;
                OnPropertyChanged(nameof(WedClock0));
            }
        }
        private String _thursClock0;

        public String ThursClock0
        {
            get { return _thursClock0; }
            set { _thursClock0 = value;
                    OnPropertyChanged(nameof(ThursClock0)); }
        }
        private String _friClock0;

        public String FriClock0
        {
            get { return _friClock0; }
            set
            {
                _friClock0 = value;
                OnPropertyChanged(nameof(FriClock0));
            }
        }
        private String _satClock0;

        public String SatClock0
        {
            get { return _satClock0; }
            set
            {
                _satClock0 = value;
                OnPropertyChanged(nameof(SatClock0));
            }
        }
        private String _sunClock0;

        public String SunClock0
        {
            get { return _sunClock0; }
            set
            {
                _sunClock0 = value;
                OnPropertyChanged(nameof(SunClock0));
            }
        }
        private String _monClock1;

        public String MonClock1
        {
            get { return _monClock1; }
            set
            {
                _monClock1 = value;
                OnPropertyChanged(nameof(MonClock1));
            }
        }
        private String _tuesClock1;

        public String TuesClock1
        {
            get { return _tuesClock1; }
            set
            {
                _tuesClock1 = value;
                OnPropertyChanged(nameof(TuesClock1));
            }
        }
        private String _wedClock1;

        public String WedClock1
        {
            get { return _wedClock1; }
            set
            {
                _wedClock1 = value;
                OnPropertyChanged(nameof(WedClock1));
            }
        }
        private String _thursClock1;

        public String ThursClock1
        {
            get { return _thursClock1; }
            set
            {
                _thursClock1 = value;
                OnPropertyChanged(nameof(ThursClock1));
            }
        }
        private String _friClock1;

        public String FriClock1
        {
            get { return _friClock1; }
            set
            {
                _friClock1 = value;
                OnPropertyChanged(nameof(FriClock1));
            }
        }
        private String _satClock1;

        public String SatClock1
        {
            get { return _satClock1; }
            set
            {
                _satClock1 = value;
                OnPropertyChanged(nameof(SatClock1));
            }
        }
        private String _sunClock1;

        public String SunClock1
        {
            get { return _sunClock1; }
            set
            {
                _sunClock1 = value;
                OnPropertyChanged(nameof(SunClock1));
            }
        }
        private String _timeGermany;

        public String TimeGermany
        {
            get { return _timeGermany; }
            set
            {
                _timeGermany = value;
                OnPropertyChanged(nameof(TimeGermany)); 
            }
        }
        private String _timeSingapore;

        public String TimeSingapore
        {
            get { return _timeSingapore; }
            set
            {
                _timeSingapore = value;
                OnPropertyChanged(nameof(TimeSingapore));
            }
        }

        public DateTime DateTimeGermany { get; set; }
        public DateTime DateTimeSingapore { get; set; }
        public bool IsToggle { get; set; }

        private bool _isGermanyNightTime;
        public bool IsGermanyNightTime
        {
            get { return this._isGermanyNightTime; }
            set
            {
                this._isGermanyNightTime = value;
                OnPropertyChanged(nameof(IsGermanyNightTime));
            }
        }

        private bool _isSingaporeNightTime;
        public bool IsSingaporeNightTime
        {
            get { return this._isSingaporeNightTime; }
            set
            {
                this._isSingaporeNightTime = value;
                OnPropertyChanged(nameof(IsSingaporeNightTime));
            }
        }


        private String _daytimeBremen;

        public String DaytimeBremen
        {
            get { return _daytimeBremen; }
            set
            {
                _daytimeBremen = value;
                OnPropertyChanged(nameof(DaytimeBremen));
            }
        }
        private String _daytimeSingapore;

        public String DaytimeSingapore
        {
            get { return _daytimeSingapore; }
            set
            {
                _daytimeSingapore = value;
                OnPropertyChanged(nameof(DaytimeSingapore));
            }
        }

        private String _secondsGermany;

        public String SecondsGermany
        {
            get { return _secondsGermany; }
            set
            {
                _secondsGermany = value;
                OnPropertyChanged(nameof(SecondsGermany));
            }
        }

        private String _amClock0;

        public String AmClock0
        {
            get { return _amClock0; }
            set
            {
                _amClock0 = value;
                OnPropertyChanged(nameof(AmClock0));
            }
        }

        private String _amClock1;

        public String AmClock1
        {
            get { return _amClock1; }
            set
            {
                _amClock1 = value;
                OnPropertyChanged(nameof(AmClock1));
            }
        }

        private String _pmClock0;

        public String PmClock0
        {
            get { return _pmClock0; }
            set
            {
                _pmClock0 = value;
                OnPropertyChanged(nameof(PmClock0));
            }
        }

        private String _pmClock1;

        public String PmClock1
        {
            get { return _pmClock1; }
            set
            {
                _pmClock1 = value;
                OnPropertyChanged(nameof(PmClock1));
            }
        }

        public String CalculateDaytimeSingapore (DateTime dateTime)
        {
            if (dateTime.Hour >= 19)
            {
                return "Night";
            }  
            else if (dateTime.Hour >= 17)
            {
                return "Evening";
            }
            else if (dateTime.Hour >= 14)
            {
                return "Afternoon";
            }
            else if (dateTime.Hour >= 12)
            {
                return "Noon";
            }
            else if (dateTime.Hour >= 8)
            {
                return "Morning";
            }
            else if (dateTime.Hour >= 7)
            {
                return "Early Morning";
            }
            else
            {
                return "Night";
            }
        }

        public String CalculateDaytimeBremen(DateTime dateTime)
        {
            if (dateTime.Hour >= 22)
            {
                return "Night";
            }
            else if (dateTime.Hour >= 19)
            {
                return "Evening";
            }
            else if (dateTime.Hour >= 16)
            {
                return "Afternoon";
            }
            else if (dateTime.Hour >= 12)
            {
                return "Noon";
            }
            else if (dateTime.Hour >= 7)
            {
                return "Morning";
            }
            else if (dateTime.Hour >= 5)
            {
                return "Early Morning";
            }
            else
            {
                return "Night";
            }
        }

        public MainViewModel()
        {
            IsToggle = true;
            /*
            this.TimeGermany = "12:24";
             * 
            DateTime utcNow = DateTime.UtcNow;
            DateTimeGermany = utcNow.Add(new TimeSpan(2,0,0));
            TimeGermany = DateTimeGermany.ToShortTimeString();

            utcNow = DateTime.UtcNow;
            DateTimeSingapore = utcNow.Add(new TimeSpan(8, 0, 0));
            TimeSingapore = DateTimeSingapore.ToShortTimeString();
            */
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            DateTime utcNow = DateTime.UtcNow;
            DateTimeGermany = utcNow.Add(new TimeSpan(2, 0, 0));
            DateTimeSingapore = utcNow.Add(new TimeSpan(8, 0, 0));
            this.DaytimeBremen = CalculateDaytimeBremen(DateTimeGermany);
            this.DaytimeSingapore = CalculateDaytimeSingapore(DateTimeSingapore);

            CalculateIsNighttime();
            CalculateHoursAndMinutes();
            CalculateSeconds();
            CalculateAmPm();
            CalculateWeekday();

        }

        private void CalculateWeekday()
        {
            this.MonClock0 = "";
            this.TuesClock0 = "";
            this.WedClock0 = "";
            this.ThursClock0 = "";
            this.FriClock0 = "";
            this.SatClock0 = "";
            this.SunClock0 = "";
            this.MonClock1 = "";
            this.TuesClock1 = "";
            this.WedClock1 = "";
            this.ThursClock1 = "";
            this.FriClock1 = "";
            this.SatClock1 = "";
            this.SunClock1 = "";
            switch ((int)(DateTimeGermany.DayOfWeek + 6) % 7)
            {
                case 0:
                    this.MonClock0 = "MON";
                    break;
                case 1:
                    this.TuesClock0 = "TUES";
                    break;
                case 2:
                    this.WedClock0 = "WED";
                    break;
                case 3:
                    this.ThursClock0 = "THURS";
                    break;
                case 4:
                    this.FriClock0 = "FRI";
                    break;
                case 5:
                    this.SatClock0 = "SAT";
                    break;
                case 6:
                    this.SunClock0 = "SUN";
                    break;
            }
            switch ((int)(DateTimeSingapore.DayOfWeek + 6) % 7)
            {
                case 0:
                    this.MonClock1 = "MON";
                    break;
                case 1:
                    this.TuesClock1 = "TUES";
                    break;
                case 2:
                    this.WedClock1 = "WED";
                    break;
                case 3:
                    this.ThursClock1 = "THURS";
                    break;
                case 4:
                    this.FriClock1 = "FRI";
                    break;
                case 5:
                    this.SatClock1 = "SAT";
                    break;
                case 6:
                    this.SunClock1 = "SUN";
                    break;
            }
        }

        private void CalculateHoursAndMinutes()
        {

            if (IsToggle)
            {
                TimeGermany = DateTimeGermany.ToString("hh");
                TimeGermany += " ";
                TimeGermany += DateTimeGermany.ToString("mm");
                TimeSingapore = DateTimeSingapore.ToString("hh");
                TimeSingapore += " ";
                TimeSingapore += DateTimeSingapore.ToString("mm");

                this.IsToggle = false;
            }
            else
            {
                TimeGermany = DateTimeGermany.ToString("hh");
                TimeGermany += ":";
                TimeGermany += DateTimeGermany.ToString("mm");
                TimeSingapore = DateTimeSingapore.ToString("hh");
                TimeSingapore += ":";
                TimeSingapore += DateTimeSingapore.ToString("mm");
                this.IsToggle = true;
            }
        }

        private void CalculateIsNighttime()
        {
            if (this.DaytimeBremen.Equals("Night"))
            {
                this.IsGermanyNightTime = true;
            }
            else
            {
                this.IsGermanyNightTime = false;
            }
            if (this.DaytimeSingapore.Equals("Night"))
            {
                this.IsSingaporeNightTime = true;
            }
            else
            {
                this.IsSingaporeNightTime = false;
            }
        }

        private void CalculateSeconds()
        {
            this.SecondsGermany = DateTimeGermany.ToString("ss");

        }

        private void CalculateAmPm()
        {
            if (this.DateTimeGermany.ToString("tt", CultureInfo.InvariantCulture).Equals("AM"))
            {
                this.AmClock0 = "AM";
            }
            else
            {
                this.AmClock0 = "";
            }
            if (this.DateTimeGermany.ToString("tt", CultureInfo.InvariantCulture).Equals("PM"))
            {
                this.PmClock0 = "PM";
            }
            else
            {
                this.PmClock0 = "";
            }
            if (this.DateTimeSingapore.ToString("tt", CultureInfo.InvariantCulture).Equals("AM"))
            {
                this.AmClock1 = "AM";
            }
            else
            {
                this.AmClock1 = "";
            }
            if (this.DateTimeSingapore.ToString("tt", CultureInfo.InvariantCulture).Equals("PM"))
            {
                this.PmClock1 = "PM";
            }
            else
            {
                this.PmClock1 = "";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
